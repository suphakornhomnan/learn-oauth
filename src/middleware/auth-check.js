export const authCheck = () => (req, res, next) => {
  if (!req.user) {
    return res.redirect("/api/auth/login");
  } else {
    next();
  }
};
