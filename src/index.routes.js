import express from "express";
import authRoutes from "./auth/auth.routes.js";
import profileRoutes from "./profile/profile.routes.js";
const router = express.Router();

router.use("/auth", authRoutes);
router.use("/profile", profileRoutes);

export default router;
