import express from "express";
import { authCheck } from "../middleware/auth-check.js";
const router = express.Router();

router.get("/", authCheck(), (req, res) => {
  return res.render("profile", { user: req.user });
});

export default router;
