import mongoose from "mongoose"

const userSchema = new mongoose.Schema({
    username: String,
    googleId: String,
    thumbnail: String
})

export const UserModel = mongoose.model("users", userSchema)