import express from "express";
import dotenv from "dotenv";
import cookieSession from "cookie-session";
import passport from "passport";
dotenv.config();
// import path from "path"
import indexRoutes from "./index.routes.js";
import { setupPassport } from "./config/setup-passport.js";
import { connectMongo } from "./config/setup-mongodb.js";
// const __dirname = path.dirname(decodeURI(new URL(import.meta.url)))
setupPassport();
const app = express();

app.set("view engine", "ejs");
app.use(
  cookieSession({
    maxAge: 24 * 60 * 60 * 1000,
    keys: [process.env.COOKIE_KEY],
  })
);
app.use(passport.initialize());
app.use(passport.session());
app.use("/api", indexRoutes);

process.env.NODE_ENV = "test" && connectMongo(process.env.MONGODB);

app.get("/", (req, res) => {
  res.render("home", { user: req.user });
});

export default app;
