import passport from "passport";
import GoogleStrategy from "passport-google-oauth20";
import dotenv from "dotenv";
import { UserModel } from "../user/user.model.js";
dotenv.config();

export const setupPassport = () => {
  passport.serializeUser((user, done) => {
    done(null, user.id);
  });

  passport.deserializeUser((id, done) => {
    UserModel.findById(id).then((user) => {
      done(null, user);
    });
  });

  passport.use(
    new GoogleStrategy(
      {
        // options for google strategy
        clientID: process.env.GOOGLE_ID,
        clientSecret: process.env.GOOGLE_SECRET,
        callbackURL: "/api/auth/google/redirect",
      },
      async (accessToken, refreshToken, profile, done) => {
        // check if user already exists in our own db
        console.log(profile._json.picture);
        const currentUser = await UserModel.findOne({ googleId: profile.id });
        if (currentUser) {
          // already have this user
          done(null, currentUser);
        } else {
          // if not, create user in our db
          const newUser = new UserModel({
            googleId: profile.id,
            username: profile.displayName,
            thumbnail: profile._json.picture
          });
          await newUser.save();
          done(null, newUser);
        }
      }
    )
  );
};
