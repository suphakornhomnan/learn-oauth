import mongoose from "mongoose";
export const connectMongo = async (url) => {
  return await mongoose.connect(
    url,
    {
      useNewUrlParser: true,
    },
    () => {
      console.log("connect mongo success");
    }
  );
};
