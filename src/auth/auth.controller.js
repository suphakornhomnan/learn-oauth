export const login = (req, res) => {
  return res.render("login", { user: req.user });
};

export const logout = (req, res) => {
  // handle with passport
  req.logout();
  return res.redirect("/");
};

export const googleAuth = (req, res) => {
  // handle with passport
  return res.send("logging in with google");
};

export const redirectGoogleAuth = (req, res) => {
  // return res.send(req.user)
  return res.redirect("/api/profile");
};
