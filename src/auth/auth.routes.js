import express from "express";
import passport from "passport";
import {
  googleAuth,
  login,
  logout,
  redirectGoogleAuth,
} from "./auth.controller.js";
const router = express.Router();

router.get("/login", login);
router.get("/logout", logout);
router.get(
  "/google",
  passport.authenticate("google", {
    scope: ["profile"],
  }),
  googleAuth
);
router.get("/google/redirect", passport.authenticate('google'),redirectGoogleAuth);

export default router;
